import Slider from './Slider';
import Pagination from './Pagination';
import Background from './Background';
import initMap from './Map';


const bgElem = document.querySelector('.header__background');
const paginationElem = document.querySelector('.pagination');
const sliderElem = document.querySelector('.slider');

const background = new Background({ elem: bgElem });
const pagination = new Pagination({ elem: paginationElem }).init();
const slider = new Slider({ elem: sliderElem });

pagination.addSubscriber('background', background.changeImage.bind(background));
pagination.addSubscriber('slider', slider.changeText.bind(slider));
pagination.autoChange(1, 4000);

window.initMap = initMap;
