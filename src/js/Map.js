export default function initMap() {
    const map = new google.maps.Map(document.getElementById('map'), {
        center: { lat: 64.132166, lng: -21.881500 },
        zoom: 18
    });
}
