export default function Pagination(options) {
    this.elem_ = options.elem;

    this.subscribers_ = {};

    this.currentPage_ = null;

    this.transition_ = null;

    let buttons = [...this.elem_.getElementsByClassName(Pagination.CssClass.BUTTON)];
    this.nextButton_ = buttons.pop();
    this.prevButton_ = buttons.shift();
    this.buttons_ = buttons;
}

Pagination.CssClass = {
    BUTTON: 'pagination__button',
    ACTIVE: 'button_active'
};

Pagination.prototype.init = function () {
    this.elem_.addEventListener('click', this.handleButtonClick_.bind(this));

    return this;
}

Pagination.prototype.addSubscriber = function (name, subscriber) {
    if (typeof subscriber !== 'function') throw new Error('Subscriber is not a function.');

    this.subscribers_[name] = subscriber;

    return () => delete this.subscribers_[name];
};

Pagination.prototype.changePage = function (pageNum, interval=2000) {
    this.toggleCssClass_(pageNum);
    this.currentPage_ = pageNum;

    if (this.transition_) clearTimeout(this.transition_);
    this.transition_ = setTimeout(() => this.transition_ = null, interval);

    if (pageNum > 0 && pageNum <= this.buttons_.length) {
        const subscribers = this.subscribers_;
        for (let key in subscribers) {
            if (subscribers.hasOwnProperty(key)) {
                subscribers[key](pageNum, interval);
            }
        }
    }
};

Pagination.prototype.getCurrentPage = function () {
    return this.currentPage_;
}

Pagination.prototype.autoChange = function (initial, interval=2000) {
    this.changePage(initial, interval);

    setInterval(() => {
        if (!this.transition_) {
            let current = this.getCurrentPage();
            current = current === this.buttons_.length ? 1 : ++current;
            this.changePage(current, interval);
        }
    }, 500);
}

Pagination.prototype.toggleCssClass_ = function (pageNum) {
    const current = this.getCurrentPage();
    if (current) {
        this.buttons_[current - 1].classList.remove(Pagination.CssClass.ACTIVE);
    }

    switch (pageNum) {
        case 1:
            this.prevButton_.disabled = true;
            this.nextButton_.disabled = false;
            this.buttons_[pageNum - 1].classList.add(Pagination.CssClass.ACTIVE);
            break;

        case this.buttons_.length:
            this.prevButton_.disabled = false;
            this.nextButton_.disabled = true;
            this.buttons_[pageNum - 1].classList.add(Pagination.CssClass.ACTIVE);
            break;

        default:
            this.prevButton_.disabled = false;
            this.nextButton_.disabled = false;
            this.buttons_[pageNum - 1].classList.add(Pagination.CssClass.ACTIVE);
        }
}

Pagination.prototype.handleButtonClick_ = function (event) {
    const page = event.target.dataset.page;

    if (typeof page === 'undefined') return;

    switch (page) {
        case 'prev':
            this.changePage(this.getCurrentPage() - 1);
            return;

        case 'next':
            this.changePage(this.getCurrentPage() + 1);
            return;

        default:
            this.changePage(parseInt(page));
            return;
    }
};
