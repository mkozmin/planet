export default function Background(options) {
    this.currentImage_ = null;
    this.elem_ = options.elem;

    this.minOpacity = options.minOpacity || '.4';
    this.maxOpacity = options.maxOpacity || '.88';
    this.tint_ = this.elem_.querySelector(`.${Background.CssClass.TINT}`);

    this.images_ = this.elem_.getElementsByClassName(Background.CssClass.IMAGE);

    this.timeoutId_ = null;
}

Background.CssClass = {
    TINT: 'header__background-tint',
    IMAGE: 'header__background-image'
};

Background.prototype.changeImage = function (pageNum, interval) {
    if (pageNum > 0 && pageNum <= this.images_.length) {
        if (this.currentImage_) {
            this.images_[this.currentImage_ - 1].style.display = 'none';
        }

        Object.assign(this.tint_.style, {
            'transition-property': 'none',
            'opacity': this.minOpacity
        });

        if (this.timeoutId_) clearTimeout(this.timeoutId_);
        this.timeoutId_ = setTimeout(() => {
            Object.assign(this.tint_.style,
                {
                    'transition-property': 'opacity',
                    'opacity': this.maxOpacity
                });
            }, interval/2);

        this.currentImage_ = pageNum;
        this.images_[pageNum - 1].style.display = 'block';
    }
};
