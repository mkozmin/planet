export default function Slider(options) {
    this.elem_ = options.elem;
    this.currentContent_ = null;

    this.title_ = this.elem_.querySelector(`.${Slider.CssClass.TITLE}`);
    this.content_ = this.elem_.getElementsByClassName(Slider.CssClass.CONTENT);
}

Slider.CssClass = {
    TITLE: 'slider__title',
    CONTENT: 'slider__content'
};

Slider.prototype.changeText = function (pageNum) {
    if (pageNum > 0 && pageNum <= this.content_.length) {
        if (this.currentContent_) {
            this.content_[this.currentContent_ - 1].style.display = 'none';
        }
        this.currentContent_ = pageNum;
        this.content_[pageNum - 1].style.display = 'block';
    }
};
