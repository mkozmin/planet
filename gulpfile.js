const path = require('path');
const gulp = require('gulp');
const concat = require('gulp-concat');
const sass = require('gulp-sass');
const sourcemaps = require('gulp-sourcemaps');
const source = require('vinyl-source-stream');
const buffer = require('vinyl-buffer');
const browserify = require('browserify');
const watchify = require('watchify');
const babel = require('babelify');


function compile(watch) {
  var bundler = watchify(browserify('./src/js/index.js', { debug: true }).transform(babel));

  function rebundle() {
    bundler.bundle()
      .on('error', function(err) { console.error(err); this.emit('end'); })
      .pipe(source('bundle.js'))
      .pipe(buffer())
      .pipe(sourcemaps.init({ loadMaps: true }))
      .pipe(sourcemaps.write('./'))
      .pipe(gulp.dest('./static'));
  }

  if (watch) {
    bundler.on('update', function() {
      console.log('-> bundling...');
      rebundle();
    });
  }

  rebundle();
}

function watchJS() {
  return compile(true);
};


gulp.task('watchJS', () => watchJS());



const cssDir = path.join(__dirname, 'src', 'css', '**', '*.scss');
gulp.task('sass', () => {
    return gulp.src(cssDir)
               .pipe(sass().on('error', sass.logError))
               .pipe(concat('styles.css'))
               .pipe(gulp.dest('./static'));
});


gulp.task('dev', ['watchJS'], () => {
    gulp.watch(cssDir, ['sass']);
});
