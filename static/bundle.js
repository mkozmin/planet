(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = Background;
function Background(options) {
    this.currentImage_ = null;
    this.elem_ = options.elem;

    this.minOpacity = options.minOpacity || '.4';
    this.maxOpacity = options.maxOpacity || '.88';
    this.tint_ = this.elem_.querySelector('.' + Background.CssClass.TINT);

    this.images_ = this.elem_.getElementsByClassName(Background.CssClass.IMAGE);

    this.timeoutId_ = null;
}

Background.CssClass = {
    TINT: 'header__background-tint',
    IMAGE: 'header__background-image'
};

Background.prototype.changeImage = function (pageNum, interval) {
    var _this = this;

    if (pageNum > 0 && pageNum <= this.images_.length) {
        if (this.currentImage_) {
            this.images_[this.currentImage_ - 1].style.display = 'none';
        }

        Object.assign(this.tint_.style, {
            'transition-property': 'none',
            'opacity': this.minOpacity
        });

        if (this.timeoutId_) clearTimeout(this.timeoutId_);
        this.timeoutId_ = setTimeout(function () {
            Object.assign(_this.tint_.style, {
                'transition-property': 'opacity',
                'opacity': _this.maxOpacity
            });
        }, interval / 2);

        this.currentImage_ = pageNum;
        this.images_[pageNum - 1].style.display = 'block';
    }
};

},{}],2:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = initMap;
function initMap() {
    var map = new google.maps.Map(document.getElementById('map'), {
        center: { lat: 64.132166, lng: -21.881500 },
        zoom: 18
    });
}

},{}],3:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = Pagination;

function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } else { return Array.from(arr); } }

function Pagination(options) {
    this.elem_ = options.elem;

    this.subscribers_ = {};

    this.currentPage_ = null;

    this.transition_ = null;

    var buttons = [].concat(_toConsumableArray(this.elem_.getElementsByClassName(Pagination.CssClass.BUTTON)));
    this.nextButton_ = buttons.pop();
    this.prevButton_ = buttons.shift();
    this.buttons_ = buttons;
}

Pagination.CssClass = {
    BUTTON: 'pagination__button',
    ACTIVE: 'button_active'
};

Pagination.prototype.init = function () {
    this.elem_.addEventListener('click', this.handleButtonClick_.bind(this));

    return this;
};

Pagination.prototype.addSubscriber = function (name, subscriber) {
    var _this = this;

    if (typeof subscriber !== 'function') throw new Error('Subscriber is not a function.');

    this.subscribers_[name] = subscriber;

    return function () {
        return delete _this.subscribers_[name];
    };
};

Pagination.prototype.changePage = function (pageNum) {
    var _this2 = this;

    var interval = arguments.length <= 1 || arguments[1] === undefined ? 2000 : arguments[1];

    this.toggleCssClass_(pageNum);
    this.currentPage_ = pageNum;

    if (this.transition_) clearTimeout(this.transition_);
    this.transition_ = setTimeout(function () {
        return _this2.transition_ = null;
    }, interval);

    if (pageNum > 0 && pageNum <= this.buttons_.length) {
        var subscribers = this.subscribers_;
        for (var key in subscribers) {
            if (subscribers.hasOwnProperty(key)) {
                subscribers[key](pageNum, interval);
            }
        }
    }
};

Pagination.prototype.getCurrentPage = function () {
    return this.currentPage_;
};

Pagination.prototype.autoChange = function (initial) {
    var _this3 = this;

    var interval = arguments.length <= 1 || arguments[1] === undefined ? 2000 : arguments[1];

    this.changePage(initial, interval);

    setInterval(function () {
        if (!_this3.transition_) {
            var current = _this3.getCurrentPage();
            current = current === _this3.buttons_.length ? 1 : ++current;
            _this3.changePage(current, interval);
        }
    }, 500);
};

Pagination.prototype.toggleCssClass_ = function (pageNum) {
    var current = this.getCurrentPage();
    if (current) {
        this.buttons_[current - 1].classList.remove(Pagination.CssClass.ACTIVE);
    }

    switch (pageNum) {
        case 1:
            this.prevButton_.disabled = true;
            this.nextButton_.disabled = false;
            this.buttons_[pageNum - 1].classList.add(Pagination.CssClass.ACTIVE);
            break;

        case this.buttons_.length:
            this.prevButton_.disabled = false;
            this.nextButton_.disabled = true;
            this.buttons_[pageNum - 1].classList.add(Pagination.CssClass.ACTIVE);
            break;

        default:
            this.prevButton_.disabled = false;
            this.nextButton_.disabled = false;
            this.buttons_[pageNum - 1].classList.add(Pagination.CssClass.ACTIVE);
    }
};

Pagination.prototype.handleButtonClick_ = function (event) {
    var page = event.target.dataset.page;

    if (typeof page === 'undefined') return;

    switch (page) {
        case 'prev':
            this.changePage(this.getCurrentPage() - 1);
            return;

        case 'next':
            this.changePage(this.getCurrentPage() + 1);
            return;

        default:
            this.changePage(parseInt(page));
            return;
    }
};

},{}],4:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = Slider;
function Slider(options) {
    this.elem_ = options.elem;
    this.currentContent_ = null;

    this.title_ = this.elem_.querySelector('.' + Slider.CssClass.TITLE);
    this.content_ = this.elem_.getElementsByClassName(Slider.CssClass.CONTENT);
}

Slider.CssClass = {
    TITLE: 'slider__title',
    CONTENT: 'slider__content'
};

Slider.prototype.changeText = function (pageNum) {
    if (pageNum > 0 && pageNum <= this.content_.length) {
        if (this.currentContent_) {
            this.content_[this.currentContent_ - 1].style.display = 'none';
        }
        this.currentContent_ = pageNum;
        this.content_[pageNum - 1].style.display = 'block';
    }
};

},{}],5:[function(require,module,exports){
'use strict';

var _Slider = require('./Slider');

var _Slider2 = _interopRequireDefault(_Slider);

var _Pagination = require('./Pagination');

var _Pagination2 = _interopRequireDefault(_Pagination);

var _Background = require('./Background');

var _Background2 = _interopRequireDefault(_Background);

var _Map = require('./Map');

var _Map2 = _interopRequireDefault(_Map);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var bgElem = document.querySelector('.header__background');
var paginationElem = document.querySelector('.pagination');
var sliderElem = document.querySelector('.slider');

var background = new _Background2.default({ elem: bgElem });
var pagination = new _Pagination2.default({ elem: paginationElem }).init();
var slider = new _Slider2.default({ elem: sliderElem });

pagination.addSubscriber('background', background.changeImage.bind(background));
pagination.addSubscriber('slider', slider.changeText.bind(slider));
pagination.autoChange(1, 4000);

window.initMap = _Map2.default;

},{"./Background":1,"./Map":2,"./Pagination":3,"./Slider":4}]},{},[5])


//# sourceMappingURL=bundle.js.map
